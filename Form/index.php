<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Index FORM</title>
</head>

<body>

<h1>Index . php</h1>
    <?php 
    if (!isset($_POST['prenom']) 
        || !isset($_POST['nom']) 
        || $_POST['prenom'] === ""
        || $_POST['nom'] === "") 
    {?>

    <form method="POST" action="/index.php" id="formulaire">
        <input type="text" name="prenom" placeholder="Prénom"><br><br>
        <input type="text" name="nom" placeholder="Nom"><br><br>
        <select name="genre">
            <option value="homme">Homme</option>
            <option value="femme">Femme</option>
        </select>
        <label for="truc">Ici on met un fichier .png :</label>
        <input type="file" id="truc" name="truc" accept=".pdf">
        <input type="submit" value="Envoyer le formulaire">
    </form>

    <?php 
    } ?>

    <?php 
    if ((isset($_POST['prenom']) 
        && $_POST['prenom']!=="") 
        && (isset($_POST['nom']) 
        && $_POST['nom']!=="" )) {
            $kkc = pathinfo($_POST['truc']);
            echo ("Salut ". $_POST['prenom']." ". $_POST['nom']." toi être ". 
            $_POST['genre']. ", ton fichier s'appelle ". $kkc['filename']. 
            " , et il est de type ". $kkc['extension']. ". Bisous partout.");
    } ?>

</body>
</html>